// GLOBAL VARS =====================================================
const player = {
    guesses: 0,
    correct: 0,
    winningWord: '',
    win: false
} 

const words = ['dogs', 'cat', 'liver', 'music', 'food', 'cars', 'stop', 'move']

// DOM ELEMENTS ================================================
const alphaContainer = document.querySelector('.alpha-container')
const buttonControl = document.querySelector('.control')
const letterOutputContainer = document.querySelector('.letter-output-container')
const imgGallows = document.querySelector('.img-gallows')
// MODAL END CONTENT ===========================================
const modalContainer = document.querySelector('.modal-toggle')
const finalMSG = document.querySelector('.final-msg')
const playAgain = document.querySelector('.play-again')
const finalIMG = document.querySelector('.final-img')


// GAME FUNCTIONS ===============================================
function startGame() {
    // GETS ALL ALPHABET BUTTONS
    const buttons = document.querySelectorAll('.single-letter')
    buttons.forEach(button => {
        button.classList.remove('dim')
        button.disabled = false;
    });
    imgGallows.classList.remove('dim')
    // REMOVE START BUTTON
    buttonControl.classList.add('dim')
    buttonControl.disabled = true
    buttonControl.style.visibility = 'hidden';
    // GET RANDOM WORD AND PLACE IN PLAYER GLOBAL VAR
    const randomWord = getRandomWord(words).split('')
    // GENERATE BLANK SPACES FOR CHOSEN WORD AND APPEND
    randomWord.forEach(letter => {
        const div = document.createElement('div')
        div.classList.add('letter-output')
        div.correctLetter = letter
        div.textContent = '_'
        letterOutputContainer.appendChild(div)
    });
}

function handleClick(e) {
    if(e.target.classList.contains('single-letter')) {
        const btn = e.target
        btn.disabled = true
        btn.classList.add('dim')
        const btnValue = e.target.myLetter
        const foundMatch = runLetterCheck(btnValue)
        if(!foundMatch) {
            player.guesses++
            imgGallows.src= `images/hang-${player.guesses + 1}.png`   
        }

        if(player.guesses === 6) {
            player.win = false
            endGameMessage('You Lost!!!')
            return
        }
        
        if(player.correct === player.winningWord.length) {
            player.win = true
            endGameMessage('You Win!!!')
            return   
        }
    }
}

function runLetterCheck(userLetter) {
    // Will Return False if no matched letters
    let final = false
    // Grab all hidden word
    const boardLetters = document.querySelectorAll('.letter-output')
    boardLetters.forEach((letter) => {
       if(userLetter === letter.correctLetter) {
           letter.textContent = userLetter
           player.correct++
           final = true
       } 
    });
    return final 
}

function endGameMessage(msg) {
    const alpha = document.querySelectorAll('.single-letter')
    const winword = document.querySelector('.winning-word')
    winWordSpan = document.createElement('span')
    winWordSpan.classList.add('winning-word-span')
    finalMSG.textContent = msg
    if(player.win) {
        finalIMG.src = 'images/Smiling_Face_Emoji_large.png'
        finalIMG.style.width = '200px'
        winWordSpan.style.color = 'rgb(89, 149, 218)';
    } else {
        finalIMG.src = 'images/hang-7.png'  
        finalIMG.style.width = '300px' 
        winWordSpan.style.color = 'rgb(231, 10, 10)';
    }
    winWordSpan.textContent = player.winningWord
    winword.textContent = `The winning word was `
    setTimeout(() => {
        modalContainer.classList.add('modal-toggle-show')
    }, 1000)
    winword.appendChild(winWordSpan)
}

function getRandomWord(arr) {
    const randomNum = Math.floor(Math.random() * arr.length)
    player.winningWord = arr[randomNum]
    return arr[randomNum] 
}


// EVENT LISTENERS ===========================================
// Page Loads And Generates Alphabet ---START HERE
window.addEventListener('load', () => {
    let counter = 97;
    while(counter < 123) {
        let button = document.createElement('button')
        let alphaletter = String.fromCharCode(counter)
        button.textContent = alphaletter    
        button.classList.add('single-letter')
        button.classList.add('dim')
        button.myLetter = alphaletter
        button.disabled = true;
        alphaContainer.appendChild(button)
        counter++
    }
})

alphaContainer.addEventListener('click', handleClick)
buttonControl.addEventListener('click', startGame)

playAgain.addEventListener('click', () => {
    imgGallows.src = 'images/hang-1.png'
    modalContainer.classList.remove('modal-toggle-show')
    // RESET PLAYER PROPS ===============
    player.guesses = 0
    player.correct = 0
    player.winningWord = 0
    player.win = false

    letterOutputContainer.innerHTML = ''
    const alphabet = document.querySelectorAll('.single-letter')
    alphabet.forEach(button => {
        button.disabled = false
        button.classList.remove('dim')
    });
    // modalContainer.style.display = 'none';
    startGame()
})